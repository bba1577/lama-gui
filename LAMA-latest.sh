!/bin/bash
#
# PLEASE DO NOT MOVE THE LINES BELOW 
gitlabtoken=""
gitlabdownloadtoken=""
currentcorpus=""
# PLEASE DO NOT MOVE THE LINES ABOVE

version="05.06.2024"

function download_corpus_services() {
      ( echo "50"
      echo " # Downloading Corpus Services..."
      if  [ "$(git ls-remote 2> /dev/null)" ]; then cd ..; fi
      curl --header "PRIVATE-TOKEN: ${gitlabdownloadtoken}" 'https://gitlab.rrz.uni-hamburg.de/api/v4/projects/3778/jobs/artifacts/main/download?job=compile_withmaven' --output artifact.zip; unzip artifact.zip; rm artifact.zip; rm -f corpus-services-latest.jar; mv target/* corpus-services-latest.jar; rmdir target
      cd $directory
      echo "100"; echo "# Done"; sleep 1
      ) |
      zenity --progress \
      --title="Downloading..." \
      --text="Downloading in progress..." \
      --percentage=0 --auto-close
}

function clone_corpus() {
      cd $directory
      gettoken="$gitlabtoken"
      clone=$(zenity --info --width=360 --height=240 \
            --text=$"Select a GitLab repository to clone here. \nYour token is: $gettoken" --extra-button="Clone without LFS" \
            --extra-button="Full clone" --extra-button="Set new token" --extra-button="List all repos" --ok-label Skip)
            if [[ $clone = "Set new token" ]]; then
                  gettoken=$(zenity --forms --title="GitLab token" \
                  --text="Enter your GitLab token here" \
                  --add-entry "GitLab token:")
                  if [ $? -eq 1 ]; then return 255; fi
                  tokenstring="gitlabtoken=\"${gettoken}\""
                  awk -v select="${tokenstring}" 'NR==4 {$0=select} 1' $0 >> LAMA-temp.sh; rm $0; mv LAMA-temp.sh $0;
                  bash $0 & exit 0
            elif [[ $clone = "List all repos" ]]; then
                  testList=$(curl --header "PRIVATE-TOKEN: $gitlabdownloadtoken" "https://gitlab.rrz.uni-hamburg.de/api/v4/projects?simple=true&min_access_level=10" | jq -r '.[] | .http_url_to_repo')
                  testarray=($(echo $testList | tr " " "\n"))
                  echo ${testarray[@]}
                  repoSelection=$(zenity --list --title "Select a repository to clone" --width=640 --height=480 --column "URL" "${testarray[@]}")
                  (
                  echo "50"
                  echo "# Cloning started"; sleep 1
                  repourl="https://oauth2:${gettoken}@${repoSelection:8}"
                  git clone $repourl
                  if [ ! $? -eq 0 ]; then 
                        clonefailed=$(zenity --warning --text="Failed to clone. Check that the token you provided is correct and try again." \
                        --ok-label Quit)
                        return 255
                  fi
                  echo "100"
                  echo "# Done"; sleep 2
                  ) | zenity --progress \
                  --title="Cloning" \
                  --text="Cloning in progress..." \
                  --percentage=0 --auto-close
            elif [[ $clone = "Full clone" ]] || [[ $clone = "Clone without LFS" ]]; then
                  chooseRepo=$(zenity --list --width=512 --height=512 \
                  --radiolist --title 'Corpus Selection' \
                  --text "Choose a corpus to clone from the list:" \
                  --column="Select" --column="Name" \
                  FALSE "INEL Dolgan Corpus" \
                  FALSE "INEL Enets Corpus" \
                  FALSE "INEL Evenki Corpus" \
                  FALSE "INEL Kamas Corpus" \
                  FALSE "INEL Mansi Corpus" \
                  FALSE "INEL Nenets Corpus" \
                  FALSE "INEL Nganasan Corpus" \
                  FALSE "INEL Selkup Corpus")
                  if [[ -n ${chooseRepo} ]]; then
                        if [[ ${chooseRepo} == "INEL Dolgan Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/dolgan.git"
                              corpname="dolgan"
                        elif [[ ${chooseRepo} == "INEL Enets Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/enets.git"
                              corpname="enets"
                        elif [[ ${chooseRepo} == "INEL Evenki Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/evenki.git"
                              corpname="evenki"
                        elif [[ ${chooseRepo} == "INEL Kamas Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/kamas.git"
                              corpname="kamas"
                        elif [[ ${chooseRepo} == "INEL Mansi Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/mansi.git"
                              corpname="mansi"      
                        elif [[ ${chooseRepo} == "INEL Nenets Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/nenets.git"
                              corpname="nenets"
                        elif [[ ${chooseRepo} == "INEL Nganasan Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/nganasan.git"
                              corpname="nganasan"
                        elif [[ ${chooseRepo} == "INEL Selkup Corpus" ]]; then
                              cloneurl="https://oauth2:${gettoken}@gitlab.rrz.uni-hamburg.de/inel/corpora/selkup.git"
                              corpname="selkup"
                        fi
                        (
                        echo "25"
                        echo "# Cloning started"; sleep 1
                        if [[ $clone = "Full clone" ]]; then
                              git clone $cloneurl $corpname
                              if [ ! $? -eq 0 ]; then 
                                    clonefailed=$(zenity --warning --text="Failed to clone. Check that the token you provided is correct and try again." \
                                    --ok-label Quit)
                                    return 255
                              fi
                        elif [[ $clone = "Clone without LFS" ]]; then
                              GIT_LFS_SKIP_SMUDGE=1 git clone $cloneurl $corpname
                              if [ ! $? -eq 0 ]; then 
                                    clonefailed=$(zenity --warning --text="Failed to clone. Check that the token you provided is correct and try again." \
                                    --ok-label Quit)
                                    return 255
                              fi
                        fi
                        echo "75"
                        echo "# Cleaning up"; sleep 1
                        #[ -f "$(dirname $0)/temp.sh" ] && { bash temp.sh; rm temp.sh; }
                        #keepcopy=$(zenity --info --text="Do you want to keep a copy of LAMA in the current folder?" --ok-label No --extra-button Yes)
                        #if [ $? -eq 1 ]; then
                        #      cp $0 $corpname
                        #else
                        #      mv $0 $corpname
                        #fi
                        echo "100"
                        echo "# Done"; sleep 2
                        ) |
                        zenity --progress \
                        --title="Cloning" \
                        --text="Cloning in progress..." \
                        --percentage=0 --auto-close
                        local corpstring="currentcorpus=\"${corpname}\""
                        awk -v select="${corpstring}" 'NR==6 {$0=select} 1' $0 >> LAMA-temp.sh; rm $0; mv LAMA-temp.sh $0;
                        bash $0 & exit 0
                  else return 255
                  fi
            else return 255
            fi
}

function select_corpus() {
      cd ${directory}
      corplist=($(find $(dirname $0) -maxdepth 2 -type f -name "*.coma" -printf "%h " ))
      corpselection=$(zenity --entry --title "Window title" --text "Insert your choice." "${corplist[@]}")
      if [[ $? -eq 0 ]]; then
            cd ${directory}
            corpselection=${corpselection#*/}       
            corpstring="currentcorpus=\"${corpselection}\""
            awk -v select="${corpstring}" 'NR==6 {$0=select} 1' $0 >> LAMA-temp.sh; rm $0; mv LAMA-temp.sh $0;
            bash $0 & exit 0
      else cd ${directory}/${currentcorpus}
      fi
}

function restore_changes() {
      restoreselection=$(zenity --info --title 'Select one' \
            --text "Do you want to restore changes in all files or some specific file?\n<b>Warning</b>: Doing so will irrevertably erase local changes you've made since last commit" \
            --extra-button 'Restore all changes' \
            --extra-button 'Select a file to restore' \
            --ok-label 'Back')
      if [[ $restoreselection = "Restore all changes" ]]; then
            git restore .
      elif [[ $restoreselection = "Select a file to restore" ]]; then
            local FILE=$(zenity --file-selection --title 'Select a file' \
                              --filename=$directory --file-filter='All files | *')
            git restore ${FILE}
      fi
      if [ $? -eq 1 ]; then
            zenity --info --text "Changes restored."
      fi
}

function advanced_lama() {
      advoptioninput=1 # OK button return code =0 , all others =1
      while [ $advoptioninput -eq 1 ]; do
            advoption=$(zenity --info --title 'LAMA' \
                  --width=240 --height=320 \
                  --text=$"<b>You're working on <span foreground='#00994C'>${currentcorpus}</span></b>\n\n\
Choose an action: \n\
<b>Confign</b>: View and set up your configuration\n\
<b>New corpus</b>: Download a new corpus\n\
<b>Download a file</b>: Download a PDF, an audio- or a video file (only useful in corpora cloned without LFS)\n\
<b>Corpus Services</b>: Select a Corpus Services function to run\n\
<b>Restore</b>: Revert your local changes (use with caution, this is not reversible)\n\
<b>Update</b>: Update Corpus Services, git or LAMA itself\n\n\
<b>Back</b>: Go back to the main menu"\
                  --extra-button 'Config' \
                  --extra-button 'New corpus' \
                  --extra-button 'Download a file' \
                  --extra-button 'Corpus Services' \
                  --extra-button 'Restore' \
                  --extra-button 'Update' \
                  --ok-label 'Back')
            advoptioninput=$?
            if [[ $advoption = "Config" ]]; then
                  cfginput=1
                  while [ $cfginput -eq 1 ]; do
                        config=$(zenity --info --width=240 --height=240 \
                        --text=$"<b>Your username is:</b> $(git config user.name) \n<b>Your email is:</b> $(git config user.email)"\
                        --extra-button Change)
                        cfginput=$?
                        if [[ $config = "Change" ]]; then 
                              changeconfig=$(zenity --forms \
                                    --title="Change your configuration" --text="Enter your username and email"\
                                    --add-entry="Enter your username:" \
                                    --add-entry="Enter your email: ")
                              IFS='|'; arrCH=(${changeconfig}); unset IFS
                              if [ "${arrCH[0]}" == "" ] || [ "${arrCH[1]}" == "" ]; then
                                    zenity --info --width=240 --height=240 \
                                    --text="No configuration entered, aborting"
                              else
                                    git config --global user.name "${arrCH[0]}"
                                    git config --global user.email "${arrCH[1]}"
                                    zenity --info --width=240 --height=240 \
                                    --text="Your configuration was updated"
                              fi
                        fi
                  done
            elif [[ $advoption = "New corpus" ]]; then
                  clone_corpus
            elif [[ $advoption = "Restore" ]]; then
                  restore_changes
            elif [[ $advoption = "Download a file" ]]; then
                  FILE=$(zenity --file-selection --title 'Select a file' \
                        --filename=$directory --file-filter='LFS files | *.wav *.pdf *.mp3 *.mpeg' --file-filter='All files | *')
                  if [[ -n ${FILE} ]]; then
			if [[ $OSTYPE = 'msys' ]]; then
                        	FNAME=${FILE##*\\};
			else FNAME=${FILE##*/}
			fi
                        pulllfs=$(git lfs pull --include "${FNAME}") | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>${FNAME} was downloaded.</b>";
                  fi
            elif [[ $advoption = "Corpus Services" ]]; then
                  chosenFunction=$(zenity --list --width 1024 --height 360 \
                        --radiolist --title 'Function selection' \
                        --text "Choose a function from the list." \
                        --column="Select" --column="Function" --column="Description"\
                        FALSE "Split flextext import" "Split a flextext imported from FLEX into individual texts" \
                        FALSE "Pretty printing" "Pretty print the entire corpus or a single file" \
                        FALSE "ExbSegmentationChecker" "Check the segmentation and update EXS files" \
                        FALSE "Cubo checks" "Select a coma file to manually run a battery of nightly cubo checks" \
                        FALSE "Cubo fixes" "Select a coma file to manually run a battery of nightly cubo fixes" \
                        FALSE "Normalize timeline" "Reassign IDs to timeline items so that they would appear in consecutive order from 1 to n" \
                        FALSE "Flextext to exb" "Convert a flextext file with one or more texts to multiple EXBs" )\
                        #FALSE ElanPunctuationChecker "Run on an ELAN file before importing it in FLEX to mark all instances of problematic punctuation marks" \
                        #FALSE FlextextPunctuationChecker "Run on a FLEX file before importing it in Partitur-Editor to replace problematic punctuation marks" \
                        #FALSE "Custom check" "Run on a coma file to perform a check on the whole corpus"
                  if [[ ${chosenFunction} == "Custom check" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a file' \
                              --filename=$directory --file-filter='Coma file | *.coma' --file-filter='All files | *')
                        if [[ -n ${FILE} ]]; then
                              checkSelection=$(zenity --forms --title="Select a function" --text="Enter the name of a function you'd like to run" \
                              --add-entry="Function")
                              runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -o $directory/curation/manual-check-output.html -s corpus-utilities/settings.param -c ${checkSelection} -n)
                        else zenity --info --width 240 --height 240 \
                              --text 'No file selected'
                        fi
                        echo "$runFunction" | zenity --progress --pulsate 
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ ${chosenFunction} == "ExbSegmentationChecker" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a folder' --filename=$directory/$currentcorpus --directory)
                        runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -p "fsm=$directory/$currentcorpus/corpus-utilities/segmentation.fsm" \
                                    -c ExbSegmentationChecker -f) | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ ${chosenFunction} == "Pretty printing" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a folder' --filename=$directory/$currentcorpus --directory)
                        runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -u PrettyPrintData -f ) | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ ${chosenFunction} == "Cubo checks" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a folder' --filename=$directory/$currentcorpus --directory)
                        runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -o $directory/$currentcorpus/curation/manual-report-output.html -p "fsm=$directory/$currentcorpus/corpus-utilities/segmentation.fsm" \
                                    -c INELChecks) | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ ${chosenFunction} == "Cubo fixes" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a folder' --filename=$directory/$currentcorpus --directory)
                        runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -o $directory/$currentcorpus/curation/manual-report-output.html -p "fsm=$directory/$currentcorpus/corpus-utilities/segmentation.fsm" \
                                    -c INELChecks -f) | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ ${chosenFunction} == "Normalize timeline" ]]; then
                        FILE=$(zenity --file-selection --title 'Select a folder' --filename=$directory/$currentcorpus --directory)
                        runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -u ExbNormalizeTimeline -f ) | zenity --progress --pulsate --auto-close
                        zenity --info --width 240 --height 240 --text "<b>Done</b>"
                  elif [[ -n ${chosenFunction} ]]; then
                        FILE=$(zenity --file-selection --title 'Select a file' \
                              --filename=$directory --file-filter='Corpus files | *.eaf *.flextext *.exb *.coma' --file-filter='All files | *')
                        if [[ -n ${FILE} ]]; then
                              if [[ ${chosenFunction} == "Split flextext import" ]]; then
                                    runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -u FlextextSplit -f) | zenity --progress --pulsate --auto-close
                              elif [[ ${chosenFunction} == "Flextext to exb" ]]; then
                                    SETTINGS=$(zenity --file-selection --title 'Select a settings file' \
                                          --filename=$directory --file-filter='Settings files | *.xsl' --file-filter='All files | *')
                                    runFunction=$(java -Xmx3g -jar $corpusServicesJar -i ${FILE} -u Flextext2EXBMultiConverter -p "settings_path=${SETTINGS}" -f) | zenity --progress --pulsate --auto-close
                              elif [[ ${chosenFunction} == "ElanPunctuationChecker" ]]; then
                                    runFunction=$(java -Xmx6g -jar $corpusServicesJar -i ${FILE} -o $directory/elan-punctuation-output.html -c ${chosenFunction} -n) | zenity --progress --pulsate --auto-close
                              elif [[ ${chosenFunction} == "FlextextPunctuationChecker" ]]; then
                                    runFunction=$(java -Xmx6g -jar $corpusServicesJar -i ${FILE} -o $directory/flextext-punctuation-output.html -c ${chosenFunction} -n -f) | zenity --progress --pulsate --auto-close
                              fi
                              echo "$runFunction" 
                              zenity --info --width 240 --height 240 --text "<b>Done</b>"
                        else zenity --info --width 240 --height 240 \
                              --text 'No file selected'
                        fi
                  else zenity --info --width 240 --height 240 \
                        --text 'No function selected'
                  fi
            elif [[ $advoption == "Update" ]]; then
                  updatewindow=$(zenity --info --width 360 --height 240 --text='Select an option' --ok-label Back \
                  --extra-button 'Update Corpus Services' --extra-button 'Update git' --extra-button 'Update LAMA')
                  if [[ $updatewindow = "Update Corpus Services" ]]; then
                        cd $directory
                        download_corpus_services | zenity --progress --pulsate --auto-close
                        cd $directory/${currentcorpus}
                  elif [[ $updatewindow = "Update git" ]]; then
                        if [[ $OSTYPE = 'msys' ]]; then
                              git update-git-for-windows
                        else zenity --info --width 240 --height 240 --text "This option is not yet supported for your OS"
                        fi
                  elif [[ $updatewindow = "Update LAMA" ]]; then
                        cd ${directory}
                        curl --header "PRIVATE-TOKEN: ${gitlabdownloadtoken}" 'https://gitlab.rrz.uni-hamburg.de/api/v4/projects/872/repository/files/lama%2FLAMA-latest%2Esh/raw?ref=main' > LAMA-temp.sh; 
                        tokenstring="gitlabtoken=\"${gitlabtoken}\""
                        corpstring="currentcorpus=\"${currentcorpus}\""
                        rm $0; mv LAMA-temp.sh $0
                        awk -v select="${tokenstring}" 'NR==4 {$0=select} 1' $0 >> LAMA-temp.sh; rm $0; mv LAMA-temp.sh $0;
                        awk -v select="${corpstring}" 'NR==6 {$0=select} 1' $0 >> LAMA-temp.sh; rm $0; mv LAMA-temp.sh $0;
                        lamaupdated=$(zenity --info --width 240 --height 240 --text "LAMA was updated" --ok-label "Reload LAMA")
                        if [ $? -eq 0 ]; then bash $0 & exit 0; fi
                  fi
            fi
      done
}

function lama() {
	  err_report() {
	 	  [ ! -d "$(dirname $0)/logs" ] && mkdir logs
	 	  if [ "$1" != "0" ]; then
	 		  echo "$(date +%Y-%m-%d_%H-%M-%S)	Error $1 on line $2" >> "$(dirname $0)/logs/LAMA-errors.log" 2>&1
	 	  fi
	     }
	  #set -o pipefail
	  set -o errtrace
	  trap 'err_report $? ${LINENO}' ERR
	
      today=$(date)
      timestamp=$(date +%Y%m%d%H%M%S)
      remote="origin" 
      conflictPath="${timestamp}-conflict.txt"
      SCRIPT=`realpath $0`
      directory=`dirname $SCRIPT`
      corpusServicesJar="${directory}/corpus-services-latest.jar"
      user=`git config user.name`
      user="${user%\"}"
      user="${user#\"}"
      conflictmessage="ATTENTION: Please resolve merge conflict manually in ${directory} by ${user}."
      pullerror="ATTENTION: The pull was faulty. Please fix it in ${directory} by ${user}."
      #jarnotfound="The JAR file is not available in ${directory} by ${user}."
      if [ ! -f "$corpusServicesJar" ]; then
            if [ -f "$(dirname $0)/corpus-services-latest.jar" ]; then
                  jarpresent=$(zenity --info --title="JAR was found" \
                  --width=240 --height=320 \
                  --text="There is a Corpus Services JAR file present. Would you like to get the latest version anyway?" \
                  --ok-label="No, get me to main menu" --extra-button="Download Corpus Services")
            else
                  getjar=$(zenity --info --title="JAR not found" \
                  --width=240 --height=320 \
                  --text="Please download Corpus Services to start working with LAMA" \
                  --ok-label="No, get me to main menu" --extra-button="Download Corpus Services")
            fi
            if [[ $getjar = "Download Corpus Services" ]] || [[ $jarpresent = "Download Corpus Services" ]]; then
                  download_corpus_services
            elif [[ $jarpresent = "No, get me to main menu" ]]; then
                  corpusServicesJar="$(dirname $0)/corpus-services-latest.jar"
            else 
                  corpusServicesJar="<span foreground='red'>ERROR: $corpusServicesJar does not exist</span>"
            fi
      fi
      if [[ ${currentcorpus} != "" ]]; then
            cd ${directory}/${currentcorpus}
      fi	
      if [ "$(git ls-remote 2> /dev/null)" ]; then
            gitrepotext="Git repository is accessible"
      else
            clone_corpus
            local cc=$?
            if [[ $cc = "255" ]]; then
                  gitrepotext="<span foreground='red'>ERROR: Git repository is not accessible</span>"
            fi
      fi
      branch=`git branch --show-current`
      optioninput=1 # OK button return code =0 , all others =1
      while [ $optioninput -eq 1 ]; do
            option=$(zenity --info --title 'LAMA' \
                  --width=240 --height=320 \
                  --text=$"<b>You're working on <span foreground='#00994C'>${currentcorpus}</span></b>\n\n\
Choose an action: \n\
<b>Status</b>: See the current state of your local repository. \n\
<b>Get updates</b>: Update your local repository \n\
<b>Push changes</b>: Save all your changes, add a message, publish your changes to the main repository and update your local repository.\n\
<b>Set working corpus</b>: Change the corpus you're currently working with\n\
<b>Help</b>: Display a help page\n\n\
<b>corpus-services:</b> ${corpusServicesJar}\n\
${gitrepotext}" \
                  --extra-button 'Status' \
                  --extra-button 'Get updates' \
                  --extra-button 'Push changes' \
                  --extra-button 'Set working corpus' \
                  --extra-button 'Advanced' \
                  --extra-button 'Help' --ok-label 'Quit')
            optioninput=$?
            if [[ $option = "Status" ]]; then
                  status=$(git status)
                  echo "$status" >> status-temp.txt
                  zenity --text-info --title="Your repository" \
                  --filename=${directory}/${currentcorpus}/status-temp.txt \
                  --width=640 --height=320
                  rm status-temp.txt
            elif [[ $option = "Get updates" ]]; then
                  CONFLICTS=$(git ls-files -u | wc -l)
                  if [ "$CONFLICTS" -gt 0 ]; then
                        git status >> $conflictPath | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
						#curl -i -X POST --data-urlencode "payload={\"text\": \"${conflictmessage}\"}" ${mattermosturl}
                        zenity --info --width=640 --height=320 \
                        --text=$"ATTENTION: The local repository cannot be updated because of a conflict.\n \
                        ${conflictmessage}\n ${conflictPath}"
                  else
                        if [ -z "$(git status --porcelain)" ]; then 
                              echo "Updating files..."
                              fetch=$(git fetch --dry-run)
                              if [ -n ${fetch} ]; then
                                    if zenity --question --text="Your repository needs an update. Proceed?" 
                                    then
                                          pullmessage=$(git pull)
                                          echo "$pullmessage" >> pull-temp.txt
                                          #echo "$pullmessage"
                                          zenity --text-info --width=640 --height=320 \
                                          --title="Update successful" \
                                          --filename=${directory}/${currentcorpus}/pull-temp.txt
                                          rm pull-temp.txt
                                    else
                                          zenity --info --width=240 --height=240 \
                                          --text="Update aborted..."
                                    fi
                              else
                                    zenity --info --width=640 --height=320 \
                                    --text="Your repository is up to date."
                              fi
                        else
                              zenity --info --width=640 --height=320 \
                              --text="ATTENTION: There are local changes. Please remove or save them with LAMA before updating."
                        fi
                  fi
            elif [[ $option = "Push changes" ]]; then
                  CONFLICTS=$(git ls-files -u | wc -l)
                  if [ "$CONFLICTS" -gt 0 ]; then
                        git status >> $conflictPath | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
						#curl -i -X POST --data-urlencode "payload={\"text\": \"${conflictmessage}\"}" ${mattermosturl}
                        zenity --info --width=640 --height=320 \
                        --text=$"ATTENTION: Please resolve a conflict before publishing your changes.\n \
                        ${conflictmessage}\n ${conflictPath}"
                  else
                        echo "Pushing local changes..."
                        if [ -z "$(git status --porcelain)" ]; then
                              zenity --info --width=640 --height=320 \
                              --text="You have no changes to push"
                        else
                              if zenity --question --text="You have local changes. Would you like to commit?"
                              then
                                    if commit=$(zenity --entry --width=640 --height=320 \
                                          --title="Commit box" \
                                          --text="Enter your commit message:" \
                                          --entry-text "Commit Message")
                                    then
                                          echo ${commit}
                                          (
                                          echo "25"
                                          echo "# Committing your changes" ; sleep 1
                                          git add -A
                                          git commit -m "${commit}"
                                          echo "50"
                                          echo "# Pretty printing the corpus" ; sleep 1

                                          java -Xmx6g -jar $corpusServicesJar -i $directory/${currentcorpus} -u PrettyPrintData -f | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
                                          git add -A 
                                          git reset -- curation/CorpusServices_Errors.xml
                                          git checkout curation/CorpusServices_Errors.xml
                                          git commit -m "Automatically pretty printed on $today" 
                                          echo "75"
                                          echo "# Pushing to remote" ; sleep 1
                                          git fetch | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
                                          git merge ${remote}/${branch} | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
                                          CONFLICTS=$(git ls-files -u | wc -l)
                                          if [ "$CONFLICTS" -gt 0 ]; then
                                                git merge --abort 
                                                echo $conflictmessage >> $conflictPath
                                                zenity --info --width=640 --height=320 \
                                                --text=$"ATTENTION: There is a merge conflict. Aborting\n \
                                                ${conflictmessage}\n ${conflictPath}"
                                                #curl -i -X POST --data-urlencode "payload={\"text\": \"${conflictmessage}\"}" ${mattermosturl}
                                                # echo "The process was stopped and LAMA will be closed."
                                                # read
                                                # exit
                                          else
                                                git push $remote $branch
                                                pushcode=$?
                                          fi
                                          echo "100"
                                          if [ ! $pushcode -eq 0 ]; then
                                                echo "# Error"; sleep 1
                                                localcommits=$(git rev-list $branch --not origin/$branch --count)
                                                git reset --soft HEAD~$localcommits
                                                zenity --info --width=240 --height=240 \
                                                      --text="<span foreground='red'>ERROR:</span> failed to push some refs. The faulty commit was undone, with local changes kept."
                                          else
                                                echo "# Done"; sleep 1
                                                if [ -z "$(git status --porcelain)" ]; then
                                                      zenity --info --width=240 --height=240 \
                                                      --text="Success"
                                                else
                                                      git status  | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
                                                      git status >> $conflictPath 2>&1 | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
                                                      echo $pullerror >> $conflictPath
                                                      zenity --info --width=640 --height=320 \
                                                      --text=$"ATTENTION: The pull was faulty. Please fix it.\n \
                                                      ${conflictmessage}\n ${conflictPath}"
                                                      #curl -i -X POST --data-urlencode "payload={\"text\": \"$pullerror\"}" ${mattermosturl}
                                                fi
                                          fi
                                          ) |
                                          zenity --progress \
                                          --title="Committing your changes..." \
                                          --text="Committing your changes..." \
                                          --height=400 --width=400 \
                                          --percentage=0 --auto-close
                                          if [ "$?" = -1 ]; then
                                                zenity --error \
                                                --text="Aborted."
                                          fi	
                                    fi
                              fi
                        fi
                  fi    
            elif [[ $option = "Set working corpus" ]]; then
                  select_corpus
            elif [[ $option = "Advanced" ]]; then
                advanced_lama
            elif [[ $option = "Help" ]]; then
                  # the indentation is weird to keep the text aligned
                  zenity --info --width=640 --height=320 \
                  --text=$"This script can be used to add changes you made to the main Git repository so everyone working with the data can see them.\n\
If there is a conflict or something goes wrong, please contact the technical team.\n\n\
<b>LAMA version:</b> ${version}\n\
<b>Git version:</b> $(git --version)\n\
<b>Latest Git version:</b> $(curl --silent https://raw.githubusercontent.com/git-for-windows/git-for-windows.github.io/main/latest-tag.txt)\n\
<b>Java version:</b> $(java --version)\n\
<b>corpus-services version:</b> ${corpusServicesJar}\n
${gitrepotext}"
            else exit 0
            fi
      done
}

[ ! -d "$(dirname $0)/logs" ] && mkdir logs

#{ 2> /dev/null 1>&2 zenity --help; } || { curl --header "PRIVATE-TOKEN: ${gitlabdownloadtoken}" 'https://gitlab.rrz.uni-hamburg.de/api/v4/projects/872/repository/files/lama%2Fzenity-3.20.0_win32-1%2Eexe/raw?ref=main' > zenity.exe; cmd="C:\Windows\System32\cmd.exe /c zenity.exe"; $cmd; rm $(dirname $0)/zenity.exe; exit 0; }

lama 2>&1 | tee -a "$(dirname $0)/logs/lama.$(date +%Y%m%d).log"
